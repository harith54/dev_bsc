<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tester extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		
		$cookie['loggedin'] = $this->session->userdata('loggedin');
		$cookie['emp_type'] = $this->session->userdata('emp_type');
		
		if($cookie['loggedin'] == TRUE){
		
			if($cookie['emp_type'] != 'tester'){
		
				redirect($cookie['emp_type'], "index");
			}
		}
		else{
		
			redirect('welcome/login');
		}
	}

	public function index()
	{
		redirect("tester/projects");
	}

	public function projects()
	{
		$this->load->model("tester_model");
		$page['projects'] = $this->tester_model->projects("testing");
		$page['bug_fixes'] = $this->tester_model->bugs("testing");
		$this->load->view('tester/projects', $page);
	}


	public function assign_bugs($project_id)
    {
    	$this->load->model("tester_model");

		$config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'zip';
        $config['max_size']             = 0;
        $this->load->library('upload', $config);


		if($this->input->method(TRUE) == "POST"){
			// fetch form data sanitized
			$projectdata['project_id'] = $this->input->post('projectid', TRUE);
			$projectdata['project_name'] = $this->input->post('project_name', TRUE);
			$projectdata['bug_description'] = $this->input->post('bug_description', TRUE);
			$projectdata['developer'] = $this->input->post('developer', TRUE);
			$projectdata['bug_files'] = "";

			// trim extra spaces
			$projectdata['project_id'] = trim($projectdata['project_id']);
			$projectdata['bug_description'] = trim($projectdata['bug_description']);

			// check if user has entered data
			if ($projectdata['project_id'] == "" || 
				$projectdata['bug_description'] == "" ||
				$projectdata['developer'] == ""
				){

				$this->session->set_flashdata('assign_bug_status_msg', "Please fill all fields");
				redirect("tester/assign_bugs/$project_id");
			}

			if ($this->upload->do_upload('bug_files'))
            {
            	$upload_data = array('upload_data' => $this->upload->data());
                $projectdata['bug_files'] = $upload_data['upload_data']['file_name'];
            }


            // project states are developing, testing, or finished
            $projectdata['status'] = "developing";

			// assign bug
			$res = $this->tester_model->assign_bug($projectdata);

			// check if project was saved to database
			if($res == 1){
				$this->session->set_flashdata('assign_bug_status', $res);
				$this->session->set_flashdata('assign_bug_status_msg', "Bug assigned.");
			}
			else{
				$this->session->set_flashdata('assign_bug_status', $res);
				$this->session->set_flashdata('assign_bug_status_msg', "Bug assign failed");
			}
			redirect("tester/assign_bugs/$project_id");
		}
		else{
			// fetch cookie data to see if new project was created in previous request cycle
			$page['assign_bug_status'] = $this->session->flashdata('assign_bug_status');
			$page['assign_bug_status_msg'] = $this->session->flashdata('assign_bug_status_msg');
 
	    	$page['project'] = $this->tester_model->get_project($project_id);
    	    $this->load->view("tester/assign_bugs", $page);
		}
    }

    public function message()
    {
    	$this->load->model('developer_model');

		$config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'zip';
        $config['max_size']             = 0;
        $this->load->library('upload', $config);


        if($this->input->method(TRUE) == "POST"){
			// fetch form data sanitized
			$projectdata['subject'] = $this->input->post('subject', TRUE);
			$projectdata['message'] = $this->input->post('message', TRUE);
			$projectdata['uploaded_file'] = "";

			// trim extra spaces
			$projectdata['subject'] = trim($projectdata['subject']);
			$projectdata['message'] = trim($projectdata['message']);

			// check if user has entered data
			if ($projectdata['subject'] == "" || 
				$projectdata['message'] == ""
				){

				$this->session->set_flashdata('message_status_msg', "Please fill all fields");
				redirect("tester/message");
			}

			if ($this->upload->do_upload('msg_files'))
            {
            	$upload_data = array('upload_data' => $this->upload->data());
                $projectdata['uploaded_file'] = $upload_data['upload_data']['file_name'];
            }


            $projectdata['sender'] = $this->session->userdata('username');


			// save/sent msg
			$res = $this->developer_model->save_message($projectdata);

			// check if project was saved to database
			if($res == 1){
				$this->session->set_flashdata('message_status', $res);
				$this->session->set_flashdata('message_status_msg', "Message sent");
			}
			else{
				$this->session->set_flashdata('message_status', $res);
				$this->session->set_flashdata('message_status_msg', "Message not sent");
			}
			redirect("tester/message");
		}
		else{
			// fetch cookie data to see if new project was created in previous request cycle
			$page['message_status'] = $this->session->flashdata('message_status');
			$page['message_status_msg'] = $this->session->flashdata('message_status_msg');

			$this->load->view('tester/message', $page);
		}
    }

    public function inbox()
	{
		$this->load->model('tester_model');
		$receiver = $this->session->userdata('username');
		$inbox['messages'] = $this->tester_model->get_messages($receiver);
		$this->load->view('tester/inbox', $inbox);
	}

	public function mailview($id)
	{
		$this->load->model('admin_model');
		$message = $this->admin_model->get_message($id);
		$page['message'] = $message[0];
		$this->load->view('tester/mailview', $page);
	}

	public function logout()
	{
		session_destroy();
		redirect("welcome/login");
	}

	public function settings()
	{
		$this->load->model('admin_model');
		if($this->input->method(TRUE) == "POST"){
			$userdata['username'] = $this->session->userdata('username');
			$userdata['current_password'] = $this->input->post('old_pass', TRUE);
			$userdata['new_password'] = $this->input->post('new_pass', TRUE);
			$userdata['confirm_new_password'] = $this->input->post('confirm_new_pass', TRUE);

			$this->admin_model->change_password($userdata);

			$this->session->set_flashdata('password_status_msg', "password changed");
			redirect("tester/settings");
		}
		else{
			$page['password_status_msg'] = $this->session->flashdata('password_status_msg');
			$this->load->view("tester/settings", $page);
		}
	}

}
