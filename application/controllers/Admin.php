<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		
		$cookie['loggedin'] = $this->session->userdata('loggedin');
		$cookie['emp_type'] = $this->session->userdata('emp_type');
		
		if($cookie['loggedin'] == TRUE){
		
			if($cookie['emp_type'] != 'admin'){
		
				redirect($cookie['emp_type']);
			}
		}
		else{
		
			redirect('welcome/login');
		}
	}

	public function index()
	{
		redirect('admin/list_projects');
	}

	public function list_projects()
	{
		$this->load->model("admin_model");
		$data['projects'] = $this->admin_model->get_projects();
		$this->load->view('admin/list_projects', $data);
	}

	public function list_users()
	{
		$this->load->model("admin_model");
		$data['users'] = $this->admin_model->get_users();
		$this->load->view('admin/list_users', $data);
	}

	public function delete_project()
	{
		$this->load->model('admin_model');

		if($this->input->method(TRUE) == "POST"){
			$project_id = $this->input->post('id', TRUE);
			$confirm_delete = $this->input->post('confirm', TRUE);
			$proj_details = $this->admin_model->get_projects($project_id);
			$project['proj_details'] = $proj_details[0];
			if($confirm_delete == "confirm"){
				$this->admin_model->delete_project($project_id);
				redirect("admin/list_projects");
			}
			else{
				$this->load->view("admin/confirm_project_deletion", $project);
			}
		}
		else{
			redirect("admin/list_projects");
		}
	}


	public function delete_user()
	{
		$this->load->model('admin_model');

		if($this->input->method(TRUE) == "POST"){
			$username = $this->input->post('username', TRUE);
			$confirm_delete = $this->input->post('confirm', TRUE);
			$user_details = $this->admin_model->get_user($username);
			$user['user_details'] = $user_details[0];
			if($confirm_delete == "confirm"){
				$this->admin_model->delete_user($username);
				redirect("admin/list_users");
			}
			else{
				$this->load->view("admin/confirm_user_deletion", $user);
			}
		}
		else{
			redirect("admin/list_users");
		}
	}

	public function settings()
	{
		$this->load->model('admin_model');
		if($this->input->method(TRUE) == "POST"){
			$userdata['username'] = $this->session->userdata('username');
			$userdata['current_password'] = $this->input->post('old_pass', TRUE);
			$userdata['new_password'] = $this->input->post('new_pass', TRUE);
			$userdata['confirm_new_password'] = $this->input->post('confirm_new_pass', TRUE);

			$this->admin_model->change_password($userdata);

			$this->session->set_flashdata('password_status_msg', "password changed");
			redirect("admin/settings");
		}
		else{
			$page['password_status_msg'] = $this->session->flashdata('password_status_msg');
			$this->load->view("admin/settings", $page);
		}
	}

	public function logout()
	{
		session_destroy();
		redirect("welcome/login");
	}

	public function create_user()
	{
		$this->load->model('admin_model');
		
		// check if form was submitted. same as $_SERVER['REQUEST_METHOD']
		if($this->input->method(TRUE) == "POST"){
			// fetch form data sanitized
			$userdata['emp_name'] = $this->input->post('emp_name', TRUE);
			$userdata['username'] = $this->input->post('username', TRUE);
			$userdata['password'] = $this->input->post('password', TRUE);
			$userdata['emp_type'] = $this->input->post('emp_type', TRUE);

			// trim extra spaces
			$userdata['emp_name'] = trim($userdata['emp_name']);
			$userdata['username'] = trim($userdata['username']);
			$userdata['password'] = trim($userdata['password']);
			$userdata['emp_type'] = trim($userdata['emp_type']);

			// check if user has entered data
			if ($userdata['emp_name'] == "" || 
				$userdata['username'] == "" || 
				$userdata['password'] == "" || 
				$userdata['emp_type'] == "-1" 
				){

				$this->session->set_flashdata('user_creation_status_msg', "Please fill all fields");
				redirect("admin/create_user");
			}

			// create user
			$res = $this->admin_model->create_user($userdata);
			$this->session->set_flashdata('user_creation_status', $res['status']);
			$this->session->set_flashdata('user_creation_status_msg', $res['msg']);
			redirect("admin/create_user");
		}
		else{
			// fetch cookie data to see if new user was created in previous request cycle
			$page['user_creation_status'] = $this->session->flashdata('user_creation_status');
			$page['user_creation_status_msg'] = $this->session->flashdata('user_creation_status_msg');

			$page['emp_types'] = $this->admin_model->emp_types();
			$this->load->view('admin/create_user', $page);
		}
	}

	public function create_project()
	{
		$this->load->model('admin_model');

		$config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'zip';
        $config['max_size']             = 0;
        $this->load->library('upload', $config);


		if($this->input->method(TRUE) == "POST"){
			// fetch form data sanitized
			$projectdata['project_name'] = $this->input->post('project_name', TRUE);
			$projectdata['project_description'] = $this->input->post('project_description', TRUE);
			$projectdata['project_developer_username'] = $this->input->post('project_developer', TRUE);
			$projectdata['project_files'] = "";

			// trim extra spaces
			$projectdata['project_name'] = trim($projectdata['project_name']);
			$projectdata['project_description'] = trim($projectdata['project_description']);
			$projectdata['project_developer_username'] = trim($projectdata['project_developer_username']);

			// check if user has entered data
			if ($projectdata['project_name'] == "" || 
				$projectdata['project_description'] == "" || 
				$projectdata['project_developer_username'] == "-1" 
				){

				$this->session->set_flashdata('project_creation_status_msg', "Please fill all fields");
				redirect("admin/create_project");
			}

			if ($this->upload->do_upload('project_files'))
            {
            	$upload_data = array('upload_data' => $this->upload->data());
                $projectdata['project_files'] = $upload_data['upload_data']['file_name'];
            }


            // project states are developing, testing, or finished
            $projectdata['project_status'] = "developing";

			// create project
			$res = $this->admin_model->create_project($projectdata);

			// check if project was saved to database
			if($res == 1){
				$this->session->set_flashdata('project_creation_status', $res);
				$this->session->set_flashdata('project_creation_status_msg', "Project created.");
			}
			else{
				$this->session->set_flashdata('project_creation_status', $res);
				$this->session->set_flashdata('project_creation_status_msg', "Project creation failed");
			}
			redirect("admin/create_project");
		}
		else{
			// fetch cookie data to see if new project was created in previous request cycle
			$page['project_creation_status'] = $this->session->flashdata('project_creation_status');
			$page['project_creation_status_msg'] = $this->session->flashdata('project_creation_status_msg');

			$page['developers'] = $this->admin_model->get_all_developers();		
			$this->load->view('admin/create_project', $page);
		}

	}

	public function message()
	{
		$this->load->model('admin_model');

		$config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'zip';
        $config['max_size']             = 0;
        $this->load->library('upload', $config);


        if($this->input->method(TRUE) == "POST"){
			// fetch form data sanitized
			$projectdata['subject'] = $this->input->post('subject', TRUE);
			$projectdata['receiver'] = $this->input->post('username', TRUE);
			$projectdata['message'] = $this->input->post('message', TRUE);
			$projectdata['uploaded_file'] = "";

			// trim extra spaces
			$projectdata['subject'] = trim($projectdata['subject']);
			$projectdata['receiver'] = trim($projectdata['receiver']);
			$projectdata['message'] = trim($projectdata['message']);

			// check if user has entered data
			if ($projectdata['subject'] == "" || 
				$projectdata['message'] == "" || 
				$projectdata['receiver'] == "-1" 
				){

				$this->session->set_flashdata('message_status_msg', "Please fill all fields");
				redirect("admin/message");
			}

			if ($this->upload->do_upload('msg_files'))
            {
            	$upload_data = array('upload_data' => $this->upload->data());
                $projectdata['uploaded_file'] = $upload_data['upload_data']['file_name'];
            }


            $projectdata['sender'] = $this->session->userdata('username');


			// save/sent msg
			$res = $this->admin_model->save_message($projectdata);

			// check if project was saved to database
			if($res == 1){
				$this->session->set_flashdata('message_status', $res);
				$this->session->set_flashdata('message_status_msg', "Message sent");
			}
			else{
				$this->session->set_flashdata('message_status', $res);
				$this->session->set_flashdata('message_status_msg', "Message not sent");
			}
			redirect("admin/message");
		}
		else{
			// fetch cookie data to see if new project was created in previous request cycle
			$page['message_status'] = $this->session->flashdata('message_status');
			$page['message_status_msg'] = $this->session->flashdata('message_status_msg');

			$page['users'] = $this->admin_model->get_users();
			$this->load->view('admin/message', $page);
		}
	}

	public function inbox()
	{
		$this->load->model('admin_model');
		$receiver = $this->session->userdata('username');
		$inbox['messages'] = $this->admin_model->get_messages($receiver);
		$this->load->view('admin/inbox', $inbox);
	}

	public function mailview($id)
	{
		$this->load->model('admin_model');
		$message = $this->admin_model->get_message($id);
		$page['message'] = $message[0];
		$this->load->view('admin/mailview', $page);
	}

}
