<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Developer extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		
		$cookie['loggedin'] = $this->session->userdata('loggedin');
		$cookie['emp_type'] = $this->session->userdata('emp_type');
		
		if($cookie['loggedin'] == TRUE){
		
			if($cookie['emp_type'] != 'developer'){
		
				redirect($cookie['emp_type']);
			}
		}
		else{
		
			redirect('welcome/login');
		}
	}

	public function index()
	{
		redirect("developer/projects");
	}

	public function projects()
	{
		$this->load->model("developer_model");
		$username = $this->session->userdata('username');
		if($this->input->method(TRUE) == "POST"){
			$projectdata['project_id'] = $this->input->post('project_name', TRUE);

			// trim extra spaces
			$projectdata['project_id'] = trim($projectdata['project_id']);
		}
		else{
			$data['projects'] = $this->developer_model->projects($username, "developing");
			$this->load->view('developer/projects', $data);
		}
	}

	public function logout()
	{
		session_destroy();
		redirect("welcome/login");
	}

	public function project_status()
	{
		$this->load->model('developer_model');

		$config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'zip';
        $config['max_size']             = 0;
        $this->load->library('upload', $config);

		if($this->input->method(TRUE) == "POST"){
			$bugfix['id'] = $this->input->post('bug_id', TRUE);
			$bugfix['bug_files'] = "";

			if ($this->upload->do_upload('bug_files'))
            {
            	$upload_data = array('upload_data' => $this->upload->data());
                $bugfix['bug_files'] = $upload_data['upload_data']['file_name'];
            }
            $bugfix['status'] = "testing";

            print_r($bugfix);

			// create project
			// $res = $this->developer_model->bugfixdata($bugfix);

			// // check if project was saved to database
			// if($res == 1){
			// 	$this->session->set_flashdata('upload_status', $res);
			// 	$this->session->set_flashdata('upload_status_msg', "Bugfix submitted.");
			// }
			// else{
			// 	$this->session->set_flashdata('upload_status', $res);
			// 	$this->session->set_flashdata('upload_status_msg', "Bugfix submission failed");
			// }
			// redirect("developer/project_status");
		}
		else{
			$username = $this->session->userdata('username');
			$status = "developing";
			$page['assigned_bugs'] = $this->developer_model->get_assigned_bugs($username, $status);
			$page['upload_status_msg'] = $this->session->flashdata('upload_status_msg');

			$this->load->view('developer/project_status', $page);
		}
	}

	public function message()
	{
		$this->load->model('developer_model');

		$config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'zip';
        $config['max_size']             = 0;
        $this->load->library('upload', $config);


        if($this->input->method(TRUE) == "POST"){
			// fetch form data sanitized
			$projectdata['subject'] = $this->input->post('subject', TRUE);
			$projectdata['message'] = $this->input->post('message', TRUE);
			$projectdata['uploaded_file'] = "";

			// trim extra spaces
			$projectdata['subject'] = trim($projectdata['subject']);
			$projectdata['message'] = trim($projectdata['message']);

			// check if user has entered data
			if ($projectdata['subject'] == "" || 
				$projectdata['message'] == ""
				){

				$this->session->set_flashdata('message_status_msg', "Please fill all fields");
				redirect("developer/message");
			}

			if ($this->upload->do_upload('msg_files'))
            {
            	$upload_data = array('upload_data' => $this->upload->data());
                $projectdata['uploaded_file'] = $upload_data['upload_data']['file_name'];
            }


            $projectdata['sender'] = $this->session->userdata('username');


			// save/sent msg
			$res = $this->developer_model->save_message($projectdata);

			// check if project was saved to database
			if($res == 1){
				$this->session->set_flashdata('message_status', $res);
				$this->session->set_flashdata('message_status_msg', "Message sent");
			}
			else{
				$this->session->set_flashdata('message_status', $res);
				$this->session->set_flashdata('message_status_msg', "Message not sent");
			}
			redirect("developer/message");
		}
		else{
			// fetch cookie data to see if new project was created in previous request cycle
			$page['message_status'] = $this->session->flashdata('message_status');
			$page['message_status_msg'] = $this->session->flashdata('message_status_msg');

			$this->load->view('developer/message', $page);
		}
	}

	public function finish($project_id=NULL)
	{
		$this->load->model('developer_model');

		$config['upload_path']          = './uploads/';
        $config['allowed_types']        = 'zip';
        $config['max_size']             = 0;
        $this->load->library('upload', $config);


		if($this->input->method(TRUE) == "POST"){
			// fetch form data sanitized
			$projectdata['project_id'] = $this->input->post('project_id', TRUE);
			$projectdata['project_files'] = "";

			// trim extra spaces
			$projectdata['project_id'] = trim($projectdata['project_id']);

			// check if user has entered data
			if ($projectdata['project_id'] == ""){

				$this->session->set_flashdata('upload_status_msg', "Please fill all fields");
				redirect("developer/finish");
			}

			if ($this->upload->do_upload('project_files'))
            {
            	$upload_data = array('upload_data' => $this->upload->data());
                $projectdata['project_files'] = $upload_data['upload_data']['file_name'];
            }


            // project states are developing, testing, or finished
            $projectdata['project_status'] = "testing";

			// create project
			$res = $this->developer_model->finish_project($projectdata);

			// check if project was saved to database
			if($res == 1){
				$this->session->set_flashdata('upload_status', $res);
				$this->session->set_flashdata('upload_status_msg', "Project submitted.");
			}
			else{
				$this->session->set_flashdata('upload_status', $res);
				$this->session->set_flashdata('upload_status_msg', "Project creation failed");
			}
			redirect("developer/finish");
		}
		else{
			// fetch cookie data to see if new project was created in previous request cycle
			$page['upload_status'] = $this->session->flashdata('upload_status');
			$page['upload_status_msg'] = $this->session->flashdata('upload_status_msg');
			$page['project_id'] = $project_id;
 
			$this->load->view('developer/finish', $page);
		}
	}

	public function inbox()
	{
		$this->load->model('developer_model');
		$receiver = $this->session->userdata('username');
		$inbox['messages'] = $this->developer_model->get_messages($receiver);
		$this->load->view('developer/inbox', $inbox);
	}

	public function mailview($id)
	{
		$this->load->model('admin_model');
		$message = $this->admin_model->get_message($id);
		$page['message'] = $message[0];
		$this->load->view('developer/mailview', $page);
	}

	public function settings()
	{
		$this->load->model('admin_model');
		if($this->input->method(TRUE) == "POST"){
			$userdata['username'] = $this->session->userdata('username');
			$userdata['current_password'] = $this->input->post('old_pass', TRUE);
			$userdata['new_password'] = $this->input->post('new_pass', TRUE);
			$userdata['confirm_new_password'] = $this->input->post('confirm_new_pass', TRUE);

			$this->admin_model->change_password($userdata);

			$this->session->set_flashdata('password_status_msg', "password changed");
			redirect("developer/settings");
		}
		else{
			$page['password_status_msg'] = $this->session->flashdata('password_status_msg');
			$this->load->view("developer/settings", $page);
		}
	}

}
