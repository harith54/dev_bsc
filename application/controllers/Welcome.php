<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		$cookie['loggedin'] = $this->session->userdata('loggedin');
		$cookie['emp_type'] = $this->session->userdata('emp_type');

		if($cookie['loggedin'] == TRUE){
			if($cookie['emp_type'] == 'admin'){
				redirect("admin/index");
			}
			elseif ($cookie['emp_type'] == 'developer') {
				redirect('developer');
			}
			elseif ($cookie['emp_type'] == 'tester') {
				redirect('tester');
			}
			else{
				$this->session->set_userdata("loggedin", FALSE);
				redirect('welcome/login');
			}
		}

	}

	// default index page
	public function index()
	{
		redirect("welcome/login");
	}

	// codeigniter session documentation
	// http://127.0.0.1/BTS/user_guide/libraries/sessions.html
	public function login()
	{
		if($this->input->method(TRUE) == "POST"){
			
			$username = $this->input->post('username', TRUE);
			$password = $this->input->post('password', TRUE);
			
			$this->load->model('login_model');

			$result = $this->login_model->authenticate($username, $password);
			// if login status is success set emp name into session and route to appropriate controller
			if($result['status']){
				$cookie = array(
					'loggedin' => $result['status'] , 
					'emp_name' => $result['data']['emp_name'] , 
					'username' => $result['data']['username'] , 
					'emp_type' => $result['data']['emp_type'] , 
					);
				$this->session->set_userdata($cookie);
				redirect("welcome");
			}
			else{
				$cookie = array(
					'loggedin' => FALSE , 
					);
				$this->session->set_userdata($cookie);

				$this->session->set_flashdata('msg', $result['msg']);
				redirect("welcome/login");
			}
		}
		else{
			$values['msg'] = $this->session->flashdata('msg');
			$this->load->view('login', $values);
		}

	}
}
