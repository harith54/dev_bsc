<!DOCTYPE html>
<html>
<head>
  <title>Bug Tracking System</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/style.css')?>">
</head>
<body>
  <nav class="navbar navbar-default">
    <div class=" container container-fluid">
      <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url('index.php/tester/projects'); ?>">Bug Tracker</a>
      </div>
      <ul class="nav navbar-nav">
        <li class="active"><a href="<?php echo base_url('index.php/tester/projects'); ?>">Projects</a></li>
        <li><a href="<?php echo base_url('index.php/tester/message'); ?>">Messages</a></li>
        <li><a href="<?php echo base_url('index.php/tester/inbox'); ?>">Inbox</a></li>
        <li><a href="<?php echo base_url('index.php/tester/settings'); ?>">Settings</a></li>

      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo base_url('index.php/tester/logout'); ?>">Logout</a></li>
      </ul>
    </div>
  </nav>
  <div class="container">
    
    <div class="card col-md-12">
    <?php
    if ($assign_bug_status_msg){
      echo $assign_bug_status_msg;
    }
    ?>
    <h3>Project name: <?php echo $project['name'] ?></h4>
    <p>Developer name: <?php echo $project['developer'] ?></p>
      <form method="post" action="<?php echo base_url('index.php/tester/assign_bugs/').$project['id'] ?>" enctype="multipart/form-data">
      <input type="hidden" name="projectid" value="<?php echo $project['id'] ?>">
      <input type="hidden" name="project_name" value="<?php echo $project['name'] ?>">
      <input type="hidden" name="developer" value="<?php echo $project['developer'] ?>">
      <textarea class="textarea form-control" name="bug_description" placeholder="bug_description"></textarea>
      Source Codes  <span><input type="file" name="bug_files"></span>
      <br/>
       <input type="submit" name="finish" value="Finish Project" class="btn btn-success">
      </form>
    </div>
    </div>

  </div>
</body>
</html>