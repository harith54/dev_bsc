<!DOCTYPE html>
<html>
<head>
  <title>Bug Tracking System</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/style.css')?>">
</head>
<body>
  <nav class="navbar navbar-default">
    <div class=" container container-fluid">
      <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url('index.php/tester/projects'); ?>">Bug Tracker</a>
      </div>
      <ul class="nav navbar-nav">
        <li class="active"><a href="<?php echo base_url('index.php/tester/projects'); ?>">Projects</a></li>
        <li><a href="<?php echo base_url('index.php/tester/message'); ?>">Messages</a></li>
        <li><a href="<?php echo base_url('index.php/tester/inbox'); ?>">Inbox</a></li>
        <li><a href="<?php echo base_url('index.php/tester/settings'); ?>">Settings</a></li>

      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo base_url('index.php/tester/logout'); ?>">Logout</a></li>
      </ul>
    </div>
  </nav>
<div class="container">
    <?php 
    foreach ($projects as $project) {
      echo '<div class="card col-md-12">';
      echo "<h4>Project Name: ".$project['name']."</h4>";
      echo "<h5>Project description: ".$project['description']."</h5>";
      echo "<h5>Project developer: ".$project['developer']."</h5>";
      echo '<a href="'.base_url("/uploads/").$project["uploaded_file"].'" class="btn btn-info btn-me">Download</a>';
      echo '<form action="'.base_url("index.php/tester/assign_bugs/").$project['id'].'">';
      echo '<input type="submit" value="Assign Bug" class="btn btn-warning btn-me left-10">';
      echo '</form>';
      echo '<input type="submit" value="Approve" class="btn btn-danger btn-me left-10">
            </div>';
      }  


      // foreach ($bug_fixes as $bug_fix) {
      // echo '<div class="card col-md-12">';
      // echo "<h4>Project Name: ".$bug_fix['project_name']."</h4>";
      // echo "<h5>bug_fix description: ".$bug_fix['bug_description']."</h5>";
      // echo "<h5>bug_fix developer: ".$bug_fix['developer']."</h5>";
      // echo '<a href="'.base_url("/uploads/").$bug_fix["bug_files"].'" class="btn btn-info btn-me">Download</a>';
      // echo '<form action="'.base_url("index.php/tester/assign_bugs/").$bug_fix['id'].'">';
      // echo '<input type="submit" value="Assign Bug" class="btn btn-warning btn-me left-10">';
      // echo '</form>';
      // echo '<input type="submit" value="Approve" class="btn btn-danger btn-me left-10">
      //       </div>';
      // }  
?>
  </div>
</body>
</html>