<!DOCTYPE html>
<html>
<head>
  <title>Bug Tracking System</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/style.css')?>">
</head>
<body>
<nav class="navbar navbar-default">
  <div class=" container container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url(); ?>">Bug Tracker</a>
    </div>
  </div>
</nav>
<div class="container">
  <div class="center-div">
    <?php
    if (isset($msg)){
      echo $msg;
    }
    ?>
  <form method="post" action="<?php echo base_url('index.php/welcome/login') ?>">
      <input type="text" name="username" placeholder="User Name" class="form-control">
      <input type="password" name="password" placeholder="Password" class="form-control">
      <button class="form-control btn btn-danger">Login</button>
      
      <span><center><a href="#">Contact admin if you forgot password.</a></center></span>
      </form>
    </div>
  </div>
</div>
</body>
</html>