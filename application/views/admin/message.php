<!DOCTYPE html>
<html>
<head>
  <title>Bug Tracking System</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/style.css')?>">
</head>
<body>
<nav class="navbar navbar-default">
  <div class=" container container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url(); ?>">Bug Tracker</a>
    </div>

    <ul class="nav navbar-nav">
      <li><a href="<?php echo base_url('index.php/admin/list_projects') ?>">List Projects</a></li>
      <li><a href="<?php echo base_url('index.php/admin/list_users') ?>">List Users</a></li>
      <li><a href="<?php echo base_url('index.php/admin/create_project') ?>">Create Project</a></li>
      <li><a href="<?php echo base_url('index.php/admin/create_user') ?>">Create User</a></li>
      <li class="active"><a href="<?php echo base_url('index.php/admin/message') ?>">Message</a></li>
      <li><a href="<?php echo base_url('index.php/admin/inbox') ?>">Inbox</a></li>
      <li><a href="<?php echo base_url('index.php/admin/settings') ?>">Settings</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="<?php echo base_url('index.php/admin/logout') ?>">Logout</a></li>
    </ul>
  </div>
</nav>
<div class="container">
    <div class="card col-md-12">
    <?php
    if($message_status_msg){
    echo $message_status_msg;
    }
    ?>
      <h3>Message</h3>
     <form method="post" action="<?php echo base_url('index.php/admin/message')?>" enctype="multipart/form-data">
        <input type="text" placeholder="Enter Subject" name="subject" class="form-control">
      <select name="username" class="form-control">
        <option value="-1">-Select An Employee</option>
        <?php
        foreach ($users as $user) {
          echo "<option value='".$user['username']."'>".$user['emp_name']."</option>";
        }
        ?>
      </select>
     <textarea placeholder="Enter Message" class="form-control textarea" name="message"></textarea>
     <input type="file" name="msg_files">
     
      <input type="submit" class="btn btn-danger form-control file_upload" value="Send">
     </form>
  </div>
</div>
</body>
</html>