<!DOCTYPE html>
<html>
<head>
  <title>Bug Tracking System</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/style.css')?>">
</head>
<body>
<nav class="navbar navbar-default">
  <div class=" container container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url(); ?>">Bug Tracker</a>
    </div>

    <ul class="nav navbar-nav">
      <li><a href="<?php echo base_url('index.php/admin/list_projects') ?>">List Projects</a></li>
      <li><a href="<?php echo base_url('index.php/admin/list_users') ?>">List Users</a></li>
      <li><a href="<?php echo base_url('index.php/admin/create_project') ?>">Create Project</a></li>
      <li><a href="<?php echo base_url('index.php/admin/create_user') ?>">Create User</a></li>
      <li><a href="<?php echo base_url('index.php/admin/message') ?>">Message</a></li>
      <li class="active"><a href="<?php echo base_url('index.php/admin/inbox') ?>">Inbox</a></li>
      <li><a href="<?php echo base_url('index.php/admin/settings') ?>">Settings</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="<?php echo base_url('index.php/admin/logout') ?>">Logout</a></li>
    </ul>
  </div>
</nav>
<div class="container">
    <div class="card col-md-12">
      <h3>Subject: <?php echo $message['subject']?></h3>
      <p>From: <?php echo $message['sender']?></p>
      <p>Message: <br><?php echo $message['message']?></p>
      <p>Attachment: <?php echo "<a href='".base_url('uploads/').$message['uploaded_file']."'>".$message['uploaded_file']."</a></p>"?>
  </div>
</div>
</body>
</html>