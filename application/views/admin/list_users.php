<!DOCTYPE html>
<html>
<head>
  <title>Bug Tracking System</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/style.css')?>">
</head>
<body>
<nav class="navbar navbar-default">
  <div class=" container container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url(); ?>">Bug Tracker</a>
    </div>

    <ul class="nav navbar-nav">
      <li><a href="<?php echo base_url('index.php/admin/list_projects') ?>">List Projects</a></li>
      <li class="active"><a href="<?php echo base_url('index.php/admin/list_users') ?>">List Users</a></li>
      <li><a href="<?php echo base_url('index.php/admin/create_project') ?>">Create Project</a></li>
      <li><a href="<?php echo base_url('index.php/admin/create_user') ?>">Create User</a></li>
      <li><a href="<?php echo base_url('index.php/admin/message') ?>">Message</a></li>
      <li><a href="<?php echo base_url('index.php/admin/inbox') ?>">Inbox</a></li>
      <li><a href="<?php echo base_url('index.php/admin/settings') ?>">Settings</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="<?php echo base_url('index.php/admin/logout') ?>">Logout</a></li>
    </ul>
  </div>
</nav>

<div class="container">
    <div class="card col-md-12">
      <h3>List of all Users</h3>
      <table class="table">
        <tr>
          <th>Name</th>
          <th>Username</th>
          <th>Role</th>
          <th>Delete</th>
        </tr>
        <?php 
        foreach ($users as $user) {
          echo "<tr>";
          echo "<td>".$user['emp_name']."</td>";
          echo "<td>".$user['username']."</td>";
          echo "<td>".$user['type']."</td>";
          echo "<form method='post' action='".base_url('index.php/admin/delete_user')."'>";
          echo "<input type='hidden' name='username' value='".$user['username']."'>";
          echo "<td><button type='submit'class='btn btn-danger'>Delete</button></td> </form>";
          echo "</tr>";
        }
        ?>
      </table>
    </div>
</div>
</body>
</html>