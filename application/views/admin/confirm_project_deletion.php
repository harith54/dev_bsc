<!DOCTYPE html>
<html>
<head>
  <title>Bug Tracking System</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/style.css')?>">
</head>
<body>
<nav class="navbar navbar-default">
  <div class=" container container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url(); ?>">Bug Tracker</a>
    </div>

    <ul class="nav navbar-nav">
      <li class="active"><a href="<?php echo base_url('index.php/admin/list_projects') ?>">List Projects</a></li>
      <li><a href="<?php echo base_url('index.php/admin/list_users') ?>">List Users</a></li>
      <li><a href="<?php echo base_url('index.php/admin/create_project') ?>">Create Project</a></li>
      <li><a href="<?php echo base_url('index.php/admin/create_user') ?>">Create User</a></li>
      <li><a href="<?php echo base_url('index.php/admin/message') ?>">Message</a></li>
      <li><a href="<?php echo base_url('index.php/admin/inbox') ?>">Inbox</a></li>
      <li><a href="<?php echo base_url('index.php/admin/settings') ?>">Settings</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="<?php echo base_url('index.php/admin/logout') ?>">Logout</a></li>
    </ul>
  </div>
</nav>

<div class="container">
    <div class="card col-md-12">
    <form method="post" action="<?php echo base_url('index.php/admin/delete_project')?>">
      <p>Delete project?</p>
      <hr>
      <p>ID: <?php echo $proj_details['id']; ?> </p>
      <p>Name: <?php echo $proj_details['name']; ?> </p>
      <p>Description: <?php echo $proj_details['description']; ?> </p>
      <p>Developer: <?php echo $proj_details['developer']; ?> </p>
      <p>Status: <?php echo $proj_details['status']; ?> </p>
      <input type="hidden" name="id" value="<?php echo $proj_details['id']; ?>">
      <input type="hidden" name="confirm" value="confirm">
      <button class="btn btn-danger">Confirm Delete</button>
    </form>
    </div>
</div>
</body>
</html>