<!DOCTYPE html>
<html>
<head>
  <title>Bug Tracking System</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/style.css')?>">
</head>
<body>
<nav class="navbar navbar-default">
  <div class=" container container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url(); ?>">Bug Tracker</a>
    </div>

    <ul class="nav navbar-nav">
      <li><a href="<?php echo base_url('index.php/admin/list_projects') ?>">List Projects</a></li>
      <li><a href="<?php echo base_url('index.php/admin/list_users') ?>">List Users</a></li>
      <li><a href="<?php echo base_url('index.php/admin/create_project') ?>">Create Project</a></li>
      <li class="active"><a href="<?php echo base_url('index.php/admin/create_user') ?>">Create User</a></li>
      <li><a href="<?php echo base_url('index.php/admin/message') ?>">Message</a></li>
      <li><a href="<?php echo base_url('index.php/admin/inbox') ?>">Inbox</a></li>
      <li><a href="<?php echo base_url('index.php/admin/settings') ?>">Settings</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="<?php echo base_url('index.php/admin/logout') ?>">Logout</a></li>
    </ul>
  </div>
</nav>
<div class="container">
    <div class="card col-md-4">
      <?php
      if($user_creation_status_msg){
        echo $user_creation_status_msg;
      }
      ?>
      <h3>Create an Employee</h3>
     <form method="post" action="<?php echo base_url('index.php/admin/create_user') ?>">
      <input type="text" placeholder="Name of Employee" name="emp_name" class="form-control">
      <input type="text" placeholder="Username" name="username" class="form-control">
      <input type="text" placeholder="Password" name="password" class="form-control">
      <select name="emp_type" class="form-control">
        <option value="-1">-Select Employee Type</option>
        <?php
          foreach ($emp_types as $emp_type) {
            echo "<option value=\"".$emp_type['id']."\">".$emp_type['type']."</option>";
          }
        ?>
      </select>
      <input type="hidden" name="form_data" value="form_values">
      <button type="submit" class="form-control btn btn-danger">Create New User</button>
     </form>
  </div>
</div>
</body>
</html>