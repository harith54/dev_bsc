<!DOCTYPE html>
<html>
<head>
  <title>Bug Tracking System</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/style.css')?>">
</head>
<body>
<nav class="navbar navbar-default">
  <div class=" container container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url(); ?>">Bug Tracker</a>
    </div>

    <ul class="nav navbar-nav">
      <li><a href="<?php echo base_url('index.php/admin/list_projects') ?>">List Projects</a></li>
      <li><a href="<?php echo base_url('index.php/admin/list_users') ?>">List Users</a></li>
      <li class="active"><a href="<?php echo base_url('index.php/admin/create_project') ?>">Create Project</a></li>
      <li><a href="<?php echo base_url('index.php/admin/create_user') ?>">Create User</a></li>
      <li><a href="<?php echo base_url('index.php/admin/message') ?>">Message</a></li>
      <li><a href="<?php echo base_url('index.php/admin/inbox') ?>">Inbox</a></li>
      <li><a href="<?php echo base_url('index.php/admin/settings') ?>">Settings</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="<?php echo base_url('index.php/admin/logout') ?>">Logout</a></li>
    </ul>
  </div>
</nav>
      
<div class="container">
  <div class="card col-md-12">
    <?php
    if($project_creation_status_msg){
    echo $project_creation_status_msg;
    }
    ?>
    <h3>Create A Project</h3>
    <form method="post" action="<?php echo base_url('index.php/admin/create_project')?>" enctype="multipart/form-data">
      <input type="text" placeholder="Name of Project" name="project_name" class="form-control">
      <textarea placeholder="Enter Project description" class="form-control textarea" name="project_description"></textarea>
      Select any project zip files to upload(optional field)
      <input type="file" name="project_files">
      <select class="form-control" name="project_developer">
        <option value="-1">-Select a developer</option>
        <?php
        foreach ($developers as $developer) {
          echo "<option value=\"".$developer['username']."\">".$developer['emp_name']."</option>";
        }
        ?>
      </select>
      <button type="submit" class="btn btn-danger">Create Project</button>
    </form>
  </div>
</div>
</body>
</html>