<!DOCTYPE html>
<html>
<head>
  <title>Bug Tracking System</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/style.css')?>">
</head>
<body>
<nav class="navbar navbar-default">
  <div class=" container container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url(); ?>">Bug Tracker</a>
    </div>

    <ul class="nav navbar-nav">
      <li class="active"><a href="<?php echo base_url('index.php/admin/list_projects') ?>">List Projects</a></li>
      <li><a href="<?php echo base_url('index.php/admin/list_users') ?>">List Users</a></li>
      <li><a href="<?php echo base_url('index.php/admin/create_project') ?>">Create Project</a></li>
      <li><a href="<?php echo base_url('index.php/admin/create_user') ?>">Create User</a></li>
      <li><a href="<?php echo base_url('index.php/admin/message') ?>">Message</a></li>
      <li><a href="<?php echo base_url('index.php/admin/inbox') ?>">Inbox</a></li>
      <li><a href="<?php echo base_url('index.php/admin/settings') ?>">Settings</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="<?php echo base_url('index.php/admin/logout') ?>">Logout</a></li>
    </ul>
  </div>
</nav>

<div class="container">
    <div class="card col-md-12">
    <h3>List of all Projects</h3>
      <table class="table">
        <tr>
          <th>Project ID</th>
          <th>Project Name</th>
          <th>Status</th>
          <th>Developer</th>
          <th>Delete</th>
        </tr>
        <?php
        foreach ($projects as $project) {
          echo "<tr>";
          echo "<td>".$project['id']."</td>";
          echo "<td>".$project['name']."</td>";
          echo "<td>".$project['status']."</td>";
          echo "<td>".$project['developer']."</td>";
          echo "<form method='post' action='".base_url('index.php/admin/delete_project')."'>";
          echo "<input type='hidden' name='id' value='".$project['id']."'>";
          echo "<td><button type='submit'class='btn btn-danger'>Delete</button></td> </form>";
          echo "</tr>";
        }
        ?>
        </form>
      </table>
    </div>
</div>
</body>
</html>