<!DOCTYPE html>
<html>
<head>
  <title>Bug Tracking System</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/style.css')?>">
</head>
<body>
   <nav class="navbar navbar-default">
    <div class=" container container-fluid">
      <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url('index.php/developer/projects'); ?>">Bug Tracker</a>
      </div>
      <ul class="nav navbar-nav">
        <li><a href="<?php echo base_url('index.php/developer/projects'); ?>">Projects</a></li>
        <li><a href="<?php echo base_url('index.php/developer/project_status'); ?>">Project Status</a></li>
        <li><a href="<?php echo base_url('index.php/developer/message'); ?>">Message</a></li>
        <li class="active"><a href="<?php echo base_url('index.php/developer/inbox'); ?>">Inbox</a></li>
        <li><a href="<?php echo base_url('index.php/developer/settings'); ?>">Settings</a></li>

      </ul>
      <ul class="nav navbar-nav navbar-right">
      <li><a href="<?php echo base_url('index.php/developer/logout') ?>">Logout</a></li>
      </ul>
    </div>
  </nav>
<div class="container">
    <div class="card col-md-12">
      <h3>Inbox</h3>
     <form action="post" action="#">
        <table class="table">
          <tr>
            <th>From</th>
            <th>Subject</th>
            <th>Date</th>
          </tr>
          <?php
          foreach ($messages as $message) {
            echo "<tr>";
            echo "<td>".$message['sender']."</td>";
            echo "<td><a href='".base_url('index.php/developer/mailview/').$message['id']."'>".$message['subject']."</a></td>";
            echo "<td>".$message['msg_time']."</td>";
            echo "</tr>";
          }
          ?>
        </table>
     </form>
  </div>
</div>
</body>
</html>