<!DOCTYPE html>
<html>
<head>
  <title>Bug Tracking System</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/style.css')?>">
</head>
<body>
   <nav class="navbar navbar-default">
    <div class=" container container-fluid">
      <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url('index.php/developer/projects'); ?>">Bug Tracker</a>
      </div>
      <ul class="nav navbar-nav">
        <li class="active"><a href="<?php echo base_url('index.php/developer/projects'); ?>">Projects</a></li>
        <li><a href="<?php echo base_url('index.php/developer/project_status'); ?>">Project Status</a></li>
        <li><a href="<?php echo base_url('index.php/developer/message'); ?>">Message</a></li>
        <li><a href="<?php echo base_url('index.php/developer/inbox'); ?>">Inbox</a></li>
        <li><a href="<?php echo base_url('index.php/developer/settings'); ?>">Settings</a></li>

      </ul>
      <ul class="nav navbar-nav navbar-right">
      <li><a href="<?php echo base_url('index.php/developer/logout') ?>">Logout</a></li>
      </ul>
    </div>
  </nav>
<div class="container">

<?php
    foreach ($projects as $project) {
      echo "<div class='card col-md-12'>";
      echo "<h3>".$project['name']."</h3>";
      echo "<p>".$project['description']."</p>";
      echo "<h5>Uploaded files: <a href='".base_url('uploads/').$project['uploaded_file']."'>".$project['uploaded_file']."</a></h5>";
      echo '<span><form method="post" action="'.base_url('index.php/developer/message').'">
        <input type="hidden" name="project_id" value="'.$project['id'].'"></input>
       <input type="submit" name="mark_complete" value="Ask doubts" class="btn btn-danger">
      </form>';
      echo '<a href="'.base_url('index.php/developer/finish/').$project['id'].'" class="btn btn-success">Finish and submit</a>
    </div></span>';
    }
?>

</div>
</body>
</html>