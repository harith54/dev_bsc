<!DOCTYPE html>
<html>
<head>
  <title>Bug Tracking System</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/style.css')?>">
</head>
<body>
   <nav class="navbar navbar-default">
    <div class=" container container-fluid">
      <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url('index.php/developer/index'); ?>">Bug Tracker</a>
      </div>
      <ul class="nav navbar-nav">
        <li class="active"><a href="<?php echo base_url('index.php/developer/index'); ?>">Projects</a></li>
        <li><a href="<?php echo base_url('index.php/developer/project_status'); ?>">Project Status</a></li>
        <li><a href="<?php echo base_url('index.php/developer/message'); ?>">Message</a></li>
        <li><a href="<?php echo base_url('index.php/developer/inbox'); ?>">Inbox</a></li>
        <li><a href="<?php echo base_url('index.php/developer/settings'); ?>">Settings</a></li>

      </ul>
      <ul class="nav navbar-nav navbar-right">
      <li><a href="<?php echo base_url('index.php/developer/logout') ?>">Logout</a></li>
      </ul>
    </div>
  </nav>


