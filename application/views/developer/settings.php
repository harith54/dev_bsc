<!DOCTYPE html>
<html>
<head>
  <title>Bug Tracking System</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/style.css')?>">
</head>
<body>
   <nav class="navbar navbar-default">
    <div class=" container container-fluid">
      <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url('index.php/developer/projects'); ?>">Bug Tracker</a>
      </div>
      <ul class="nav navbar-nav">
        <li><a href="<?php echo base_url('index.php/developer/projects'); ?>">Projects</a></li>
        <li><a href="<?php echo base_url('index.php/developer/project_status'); ?>">Project Status</a></li>
        <li><a href="<?php echo base_url('index.php/developer/message'); ?>">Message</a></li>
        <li><a href="<?php echo base_url('index.php/developer/inbox'); ?>">Inbox</a></li>
        <li  class="active"><a href="<?php echo base_url('index.php/developer/settings'); ?>">Settings</a></li>

      </ul>
      <ul class="nav navbar-nav navbar-right">
      <li><a href="<?php echo base_url('index.php/developer/logout') ?>">Logout</a></li>
      </ul>
    </div>
  </nav>

<div class="container">
    <div class="card col-md-4">
    <?php
    if($password_status_msg){
    echo $password_status_msg;
    }
    ?>
      <h3>Change Password</h3>
        <center>
        <form method="post" action="<?php echo base_url('index.php/developer/settings')?>">
          <input type="text" placeholder="Old Password" name="old_pass" class="form-control" width="100">
          <input type="text" placeholder="New Password" name="new_pass" class="form-control" width="100">
          <input type="text" placeholder="Retype New Password" name="confirm_new_pass" class="form-control" width="100">
          <input type="submit" value="Change Password" class="btn btn-danger form-control" >
          </form>
        </center>
    </div>
</div>
</body>
</html>