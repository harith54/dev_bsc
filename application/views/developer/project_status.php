<!DOCTYPE html>
<html>
<head>
  <title>Bug Tracking System</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/style.css')?>">
</head>
<body>
   <nav class="navbar navbar-default">
    <div class=" container container-fluid">
      <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url('index.php/developer/projects'); ?>">Bug Tracker</a>
      </div>
      <ul class="nav navbar-nav">
        <li><a href="<?php echo base_url('index.php/developer/projects'); ?>">Projects</a></li>
        <li class="active"><a href="<?php echo base_url('index.php/developer/project_status'); ?>">Project Status</a></li>
        <li><a href="<?php echo base_url('index.php/developer/message'); ?>">Message</a></li>
        <li><a href="<?php echo base_url('index.php/developer/inbox'); ?>">Inbox</a></li>
        <li><a href="<?php echo base_url('index.php/developer/settings'); ?>">Settings</a></li>

      </ul>
      <ul class="nav navbar-nav navbar-right">
      <li><a href="<?php echo base_url('index.php/developer/logout') ?>">Logout</a></li>
      </ul>
    </div>
  </nav>
  <div class="container">

  <?php
  if ($upload_status_msg){
    $upload_status_msg;
  }
  foreach ($assigned_bugs as $bug) {
    echo '<div class="card col-md-12">';
    echo '<h4> Project name: '.$bug['project_name'].'</h4>';
    echo  '<p> Bug description: '.$bug['bug_description'].'</p>';
    echo "Downloads: <a href='".base_url('uploads/').$bug['bug_files']."'>".$bug['bug_files']."</a>";
    echo '<form method="post" enctype="multipart/form-data" action="'.base_url('index.php/developer/project_status').'">';
    echo '<input type="hidden" value="'.$bug['id'].'" name="bug_id">';
    echo '<input type="file" name="bug_files">';
    echo  '<button type="submit" class="btn btn-success">Upload bug fixed zip file of this project</button>';
    echo  '</form>';
    echo '</div>';
  }
  ?>

  </div>
</body>
</html>