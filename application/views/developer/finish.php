<!DOCTYPE html>
<html>
<head>
  <title>Bug Tracking System</title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/style.css')?>">
</head>
<body>
   <nav class="navbar navbar-default">
    <div class=" container container-fluid">
      <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url('index.php/developer/projects'); ?>">Bug Tracker</a>
      </div>
      <ul class="nav navbar-nav">
        <li class="active"><a href="<?php echo base_url('index.php/developer/projects'); ?>">Projects</a></li>
        <li><a href="<?php echo base_url('index.php/developer/project_status'); ?>">Project Status</a></li>
        <li><a href="<?php echo base_url('index.php/developer/message'); ?>">Message</a></li>
        <li><a href="<?php echo base_url('index.php/developer/inbox'); ?>">Inbox</a></li>
        <li><a href="<?php echo base_url('index.php/developer/settings'); ?>">Settings</a></li>

      </ul>
      <ul class="nav navbar-nav navbar-right">
      <li><a href="<?php echo base_url('index.php/developer/logout') ?>">Logout</a></li>
      </ul>
    </div>
  </nav>
  <div class="container">
    <div class="card col-md-12">
    <?php 
    if ($upload_status_msg){
      echo $upload_status_msg;
    }
    ?>
    <h3>Upload source code zip file</h4>
      <br/>
      <form method="post" action="<?php echo base_url('index.php/developer/finish')?>" enctype="multipart/form-data">
      <input type="file" name="project_files">
      <input type="hidden" name="project_id" value="<?php echo $project_id ?>"></input>
       <input type="submit" name="finish" value="Upload and submit project" class="btn btn-success">
      </form>
    </div>


  </div>
</body>
</html>