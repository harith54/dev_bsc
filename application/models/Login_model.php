<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function authenticate($username, $password)
    {
        $this->db->select("username, password, emp_name, emp_types.type");
        $this->db->where('username', $username);
        $this->db->from('users');
        $this->db->join('emp_types', 'users.emp_type = emp_types.id');
        $query = $this->db->get();
        if ($query->num_rows() == 1){
            $employee = $query->row();
            $password_matches = password_verify($password, $employee->password);

            if($password_matches){
                $result['status'] = TRUE;
                $result['msg'] = "Success";

                $data['emp_name'] = $employee->emp_name;
                $data['username'] = $employee->username;
                $data['emp_type'] = $employee->type;

                $result['data'] = $data;
            }
            else{
                $result['status'] = FALSE;
                $result['msg'] = "Wrong username or password.";

            }
        }
        else{
            $result['status'] = FALSE;
            $result['msg'] = "Wrong username or password.";
        }
        return $result;
    }
}