<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function create_user($userdata)
    {
        $this->db->trans_start();
        $hashed_password = password_hash($userdata['password'], PASSWORD_BCRYPT, array('cost' => 10));
        $userdata['password'] = $hashed_password;
        // checking if username already exists in users db table
        $this->db->select("username", $userdata['username']);
        $query = $this->db->get('users');
        if($query->num_rows() == 1){
            $db_resp['msg'] = "Username exists. Try another username.";
            return $db_resp;
        }
        else{
            $this->db->insert('users', $userdata);
        }
        $this->db->trans_complete();
        $db_resp['status'] = $this->db->trans_status();
        if ($this->db->trans_status() === FALSE){
            $db_resp['msg'] = "Error creating user. Please try again.";
        }
        else{
            $db_resp['msg'] = "User created.";
        }
        return $db_resp;
    }

    public function change_password($userdata)
    {
        if($userdata['new_password'] == $userdata['confirm_new_password'])
        $hashed_password = password_hash($userdata['new_password'], PASSWORD_BCRYPT, array('cost' => 10));
        $userdata['new_password'] = $hashed_password;

        $this->db->select();
        $this->db->where('username', $userdata['username']);
        $this->db->from('users');
        $query = $this->db->get();

        if ($query->num_rows() == 1){
            $employee = $query->row();
            $password_matches = password_verify($userdata['current_password'], $employee->password);

            if($password_matches){
                $result['status'] = TRUE;
                $result['msg'] = "password changed";
                $employee->password = $userdata['new_password'];
                $this->db->where('username', $userdata['username']);
                return $this->db->update('users', $employee);
            }
            else{
                $result['status'] = FALSE;
                $result['msg'] = "Wrong username or password.";

            }
        }
        else{
            $result['status'] = FALSE;
            $result['msg'] = "Wrong username or password.";
        }
        return $result;
    }

    public function emp_types()
    {
        // $this->db->where('id >', 0);
        $query = $this->db->get('emp_types');
        return $query->result_array();
    }

    public function get_all_developers()
    {
        $this->db->select('username, emp_name');
        $this->db->where('emp_type = 1');
        $query = $this->db->get('users');
        return $query->result_array();
    }

    public function get_users()
    {
        $this->db->select('username, emp_name, type');
        $this->db->from("users");
        $this->db->join("emp_types", "users.emp_type=emp_types.id");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function save_message($message)
    {
        return $this->db->insert('messages', $message);
    }


    public function get_messages($receiver)
    {
        $this->db->where('receiver', $receiver);
        $this->db->order_by('msg_time');
        $query = $this->db->get('messages');
        return $query->result_array();
    }

    public function get_message($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('messages');
        return $query->result_array();
    }


    public function create_project($projectdata)
    {
        $prepped_data['name'] = $projectdata['project_name'];
        $prepped_data['description'] = $projectdata['project_description'];
        $prepped_data['developer'] = $projectdata['project_developer_username'];
        $prepped_data['uploaded_file'] = $projectdata['project_files'];
        $prepped_data['status'] = $projectdata['project_status'];
        $res = $this->db->insert('projects', $prepped_data);
        return $res;
    }

    public function get_projects($id=NULL)
    {
        $this->db->select("id, name, description, developer, status");
        if($id != NULL){
            $this->db->where('id', $id);
        }
        $query = $this->db->get("projects");
        return $query->result_array();
    }

    public function get_user($username)
     {
        $this->db->select("emp_name, username");
        $this->db->where('username', $username);
        $query = $this->db->get("users");
        return $query->result_array();
     }

    public function delete_project($project_id)
    {
        $this->db->where('id', $project_id);
        $this->db->delete('projects');
    }

    public function delete_user($username)
    {
        $this->db->where('username', $username);
        $this->db->delete('users');
    }

//     public function create_emp($data)
//     {
//         $this->db->select('emp_id');
//         $this->db->where('emp_id', $data['emp_id']);
//         $this->db->from('employees');
//         $query = $this->db->get();
//         if($query->num_rows() == 0){

//             $hashed_password = password_hash($data['password'], PASSWORD_BCRYPT, array('cost' => 10));
//             unset($data['emp_id']);
//             $data['password'] = $hashed_password;
//             $this->db->insert('employees', $data);
//             return True;
//         }
//         else{
//             return False;
//         }
//     }

//     public function change_password($data)
//     {
//         $emp_id = $data['cookie']['emp_id'];
//         $password = $data['current_password'];
//         $new_password = $data['new_password'];

//         $auth_check = $this->validate($emp_id, $password);
//         if($auth_check['auth_success']){
//             $hashed_password = password_hash($new_password, PASSWORD_BCRYPT, array('cost' => 10));
//             $data = array(
//                 'password' => $hashed_password, 
//                 );
//             $this->db->where('emp_id', $emp_id);
//             $this->db->update('employees', $data);
//             return True;
//         }
//         else{
//             return False;
//         }
//     }
}