<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tester_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function projects($status)
    {
        $this->db->where('status', $status);
        $query = $this->db->get('projects');
        return $query->result_array();
    }

    public function bugs($status)
    {
        $this->db->where('status', $status);
        $query = $this->db->get('bugs');
        return $query->result_array();
    }

    public function get_project($project_id)
    {
        $this->db->where('id', $project_id);
        $query = $this->db->get('projects');
        $result = $query->result_array();
        return $result[0];
    }

    public function get_messages($receiver)
    {
        $this->db->where('receiver', $receiver);
        $this->db->order_by('msg_time');
        $query = $this->db->get('messages');
        return $query->result_array();
    }

    public function assign_bug($projectdata)
    {
        return $this->db->insert('bugs', $projectdata);
    }

}