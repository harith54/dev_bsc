<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Developer_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function projects($username, $status)
    {
        $this->db->where('developer', $username);
        $this->db->where('status', $status);
        $query = $this->db->get('projects');
        return $query->result_array();
    }

    public function save_message($message)
    {
        $message['receiver'] = "aa";
        return $this->db->insert('messages', $message);
    }

    public function get_messages($receiver)
    {
        $this->db->where('receiver', $receiver);
        $this->db->order_by('msg_time');
        $query = $this->db->get('messages');
        return $query->result_array();
    }

    public function finish_project($projectdata){
        $this->db->select();
        $this->db->where('id', $projectdata['project_id']);
        $query = $this->db->get("projects");
        $project = $query->result_array();
        $project = $project[0];

        $project['uploaded_file'] = $projectdata['project_files'];
        $project['status'] = $projectdata['project_status'];
        $this->db->where('id', $projectdata['project_id']);
        return $this->db->update('projects', $project);
    }

    public function get_assigned_bugs($username, $status)
    {
        $this->db->select();
        $this->db->where('developer', $username);
        $this->db->where('status', $status);
        $query = $this->db->get('bugs');
        return $query->result_array();
    }

    public function bugfixdata($bugfix)
    {
        $this->db->select();
        $this->db->where('id', $bugfix['id']);
        $query = $this->db->get('bugs');
        $result = $query->result_array()[0];

        $result['status'] = $bugfix['status'];
        $result['bug_files'] = $bugfix['bug_files'];
        $this->db->where('id', $bugfix['id']);
        return $this->db->update('bugs', $result);
    }

}