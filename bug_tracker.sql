-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 24, 2017 at 12:40 PM
-- Server version: 5.7.17-0ubuntu0.16.04.1
-- PHP Version: 7.0.15-0ubuntu0.16.04.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bug_tracker`
--

-- --------------------------------------------------------

--
-- Table structure for table `bugs`
--

CREATE TABLE `bugs` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `project_name` varchar(50) NOT NULL,
  `bug_description` text NOT NULL,
  `bug_files` text NOT NULL,
  `developer` varchar(50) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bugs`
--

INSERT INTO `bugs` (`id`, `project_id`, `project_name`, `bug_description`, `bug_files`, `developer`, `status`) VALUES
(6, 6, 'PHP project', 'login not working', 'msg_test10.zip', 'bb', 'developing');

-- --------------------------------------------------------

--
-- Table structure for table `emp_types`
--

CREATE TABLE `emp_types` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emp_types`
--

INSERT INTO `emp_types` (`id`, `type`) VALUES
(0, 'admin'),
(1, 'developer'),
(2, 'tester');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `sender` varchar(50) NOT NULL,
  `receiver` varchar(50) NOT NULL,
  `subject` varchar(250) NOT NULL,
  `message` text NOT NULL,
  `uploaded_file` text NOT NULL,
  `msg_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `sender`, `receiver`, `subject`, `message`, `uploaded_file`, `msg_time`) VALUES
(1, 'aa', 'bb', 'Just do it', 'IMPORTANT!!!', 'msg_test1.zip', '2017-08-24 00:44:26'),
(2, 'aa', 'cc', 'confidential', 'hello', 'msg_test2.zip', '2017-08-24 00:45:09'),
(3, 'aa', 'dd', 'D', 'msg D', 'project.zip', '2017-08-24 00:46:25'),
(4, 'bb', 'aa', 'for admin', 'out msg', 'msg_test3.zip', '2017-08-24 07:26:40'),
(5, 'bb', 'aa', '123', '321', 'project4.zip', '2017-08-24 08:07:46'),
(6, 'dd', 'aa', 'billing', 'what to do admin', 'msg_test9.zip', '2017-08-24 11:26:01');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `description` text NOT NULL,
  `uploaded_file` text NOT NULL,
  `developer` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `description`, `uploaded_file`, `developer`, `status`) VALUES
(6, 'PHP project', 'Ecommerce for nobody!', 'msg_test5.zip', 'bb', 'testing'),
(7, 'aaaaaa', 'bbbbbb', '', 'bb', 'developing');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `username` varchar(50) NOT NULL,
  `emp_name` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `emp_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `emp_name`, `password`, `emp_type`) VALUES
('aa', 'A', '$2y$10$WUfaEUvObI58BVONqnk8/eehFwpgeghJ8K2/3v6XiWyfuRx2Z.sDu', 0),
('bb', 'B', '$2y$10$VdWXASc31GBWQyIe9SQXk.uMW6xmcG16k7Ikm.P.KaJ7Svi2Xq.3.', 1),
('cc', 'C', '$2y$10$FRuTUGNGQUrND1oC/ootBO9n.vIniN1Sa6ieZYCaZq1dsoCSW1C4q', 1),
('dd', 'D', '$2y$10$pmh/SIrqxiQ8eLdpGYhpnOcU2.tL4.hK2Q.uvyAABfpVaD7IDOvOC', 2),
('ee', 'E', '$2y$10$3HLYmC3HRPs6e.RxXccPjOVAUaZFMmOfKmPUR0EqPxWShDckS24JK', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bugs`
--
ALTER TABLE `bugs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emp_types`
--
ALTER TABLE `emp_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role` (`type`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bugs`
--
ALTER TABLE `bugs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `emp_types`
--
ALTER TABLE `emp_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
